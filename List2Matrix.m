function [ priceMatrix ] = List2Matrix(adjancencyMat, listStruct)
    priceMatrix = adjancencyMat * 0;
    [n, m] = size(adjancencyMat);
    for i = 1:n
        listIndex = 1;
        for j = 1:m
            if (adjancencyMat(i, j) == 1)
                priceMatrix(i, j) = listStruct(i).mu(listIndex);
                listIndex = listIndex + 1;
            end
        end
    end
end