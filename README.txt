This codebase and its associated datasets are a companion to the paper Fair Resource
Allocation in a Volatile Marketplace, by MH Bateni, Y. Chen, D.F. Ciocan and V. Mirrokni.
A description of the data and the construction of the test instances can be found in
Section 6 of the paper. The dataset adds dynamic impression arrival data to the existing
dataset of Balseiro et al 2014, whose codebase we include in the directory adx-alloc-data-2014.

The script main.m can be used to execute the re-optimization schemes.
Users who desire to use our dataset for other projects can directly invoke the
function buildInstance.m to create a struct containing a single problem instance.
