function [clairvoyantPropFairUtility, clairvoyantEfficiencyUtility] = bicriteriaClairvoyant(numPeriods, invDim, demandDim, T, valuations, inventory, budget)
% Runs both proportional fairness and efficiency offline schemes for one sample path of the inventory processes.
%
% INPUTS:
%   T:double       = length of time horizon
%   numPeriods:int = discretization parameter. i.e. number of times inventory OU
%                    process updates
%   numReopts:int  = discretization paramter of re-optimization, i.e. number of
%                    times scheme re-optimizes
%   invDim:int     = number of impression types
%   demandDim:int  = number of advertisers
%   valuations:
%       double[invDim, demandDim]  = array of valuations
%   inventory:
%       double[invDim, numPeriods] = array containing timeseries of
%                                    impression arrival processes
%   budget:double[demandDim]       = array of advertiser budgets
%
% OUTPUTS: 
%   clairvoyantPropFairUtility:
%       double[demandDim] = array with advertiser utilities garnered by 
%                           propportional fairness offline scheme
%   clairvoyantEfficiencyUtility:
%       double[demandDim] = array with advertiser utilities garnered by 
%                           efficiency offline scheme
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    deltaT = T / numPeriods;
    buyerUtilities = zeros(demandDim, 1);
    currAllocation_PF = zeros(invDim, demandDim);
    currPrices_PF = zeros(invDim, 1);
    currUtilities_PF = zeros(demandDim, 1);
    [currAllocation_PF, currPrices_PF, currUtilities_PF] = solveEGProgram(budget, inventory * deltaT, valuations, invDim, demandDim);
    [currAllocation_EF, currPrices_EF, currUtilities_EF] = solveEffProgram(inventory * deltaT, valuations, invDim, demandDim);
    clairvoyantPropFairUtility = (currAllocation_PF .* valuations)' * inventory * deltaT;
    clairvoyantEfficiencyUtility = (currAllocation_EF .* valuations)' * inventory * deltaT;
end