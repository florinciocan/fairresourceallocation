function [algOneShotUtility] = bicriteriaReoptimization(numPeriods, numReopts, invDim, demandDim, T, valuations, inventory, budget)
% Runs bi-criteria re-optimization scheme for one sample path of the inventory processes.
%
% INPUTS:
%   T:double       = length of time horizon
%   numPeriods:int = discretization parameter. i.e. number of times inventory OU
%                    process updates
%   numReopts:int  = discretization paramter of re-optimization, i.e. number of
%                    times scheme re-optimizes
%   invDim:int     = number of impression types
%   demandDim:int  = number of advertisers
%   valuations:
%       double[invDim, demandDim]  = array of valuations
%   inventory:
%       double[invDim, numPeriods] = array containing timeseries of
%                                    impression arrival processes
%   budget:double[demandDim]       = array of advertiser budgets
%
% OUTPUTS: 
%   algOneShotUtilitity:
%       double[demandDim] = array with advertiser utilities garnered by bi-criteria scheme
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    beta_ratio = 1.0;
    buyerUtilities = zeros(demandDim, 1);
    deltaT = T / numPeriods;
    timeDiff = numPeriods / numReopts;
    currAllocation_PF = zeros(invDim, demandDim);
    currPrices_PF = zeros(invDim, 1);
    currUtilities_PF = zeros(demandDim, 1);
    remaining_budget_PF = budget;
    currAllocation_EF = zeros(invDim, demandDim);
    currPrices_EF = zeros(invDim, 1);
    currUtilities_EF = zeros(demandDim, 1);
    remaining_budget_EF = budget;
    
    for i = 0 : (numReopts - 1)
        currT = i * timeDiff * deltaT;    
        [currAllocation_PF, currPrices_PF, currUtilities_PF] = solveEGProgram(remaining_budget_PF, inventory(:, i * timeDiff + 1) * (T - currT), valuations, invDim, demandDim);
        [currAllocation_EF, currPrices_EF, currUtilities_EF] = solveEffProgram(inventory(:, i * timeDiff + 1) * (T - currT), valuations, invDim, demandDim);
        currAllocation = beta_ratio * currAllocation_PF + (1 - beta_ratio) * currAllocation_EF;
        buyerUtilities = buyerUtilities + (currAllocation .* valuations)' * sum(inventory(:, i  * timeDiff + 1:(i + 1)  * timeDiff)')' * deltaT;
        remaining_budget_PF = remaining_budget_PF - budget / numReopts;
    end
    algOneShotUtility = buyerUtilities;
end