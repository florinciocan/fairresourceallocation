function [ budgets ] = buildBudgets(averageValuations, advertiserCapacities)
% Creates advertiser budgets using advertiser capacity data
%
% INPUTS:
%   averageValuations:
%       double[demandDim] = array containing average valuation of each advertiser
%   advertiserCapacities:
%       double[demandDim] = array of advertiser capacities from Balseiro et al dataset 
%
% OUTPUTS: 
%   budgets:double[demandDim] = array of advertiser budgets
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni
    %advertiserCapacities
    %averageValuations
    budgets = advertiserCapacities .* averageValuations;
end 