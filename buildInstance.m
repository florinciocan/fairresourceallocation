function [ instance ] = buildInstance(pubIndex, T, numPeriods)
% Creates a struct which loads an allocation problem instance
%
% INPUTS
% pubIndex:int = index of publisher in dataset; ranges from 1 to 6
% T:double       = length of time horizon
% numPeriods:int = discretization parameter of OU inventory process, i.e.
%                  number of times process updates
%
% OUTPUTS
% instance:struct with fields:
%   invDim:int     = number of impression types
%   demandDim:int  = number of advertisers
%   valuations:
%     double[invDim, demandDim]  = array of valuations
%   budgets:
%     int[demandDim]             = array of advertiser budgets
%   inventoryProcess:
%     double[invDim, numPeriods] = array containing timeseries of
%                                  impression arrival processes
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    rng(1); % Set RNG seed
    addpath 'adx-alloc-data-2014'
    p_to_string = int2str(pubIndex);
    adfile = strcat('pub', p_to_string, '-ads.txt');
    typefile = strcat('pub', p_to_string, '-types.txt');
    
    % Create graph and valuations from Balseiro et al dataset
    [ instance.demandDim, instance.invDim, rho, arrival_prob, adjacencyMat, quality ] = LoadSynthFile( adfile, typefile );
    instance.valuations = List2Matrix(adjacencyMat, quality);
    publisherN = [1500000, 2100000, 320000, 930000, 1800000, 6700000, 7000000];
    advertiserCapacities = rho * publisherN(pubIndex);
    inventoryMeans = arrival_prob * publisherN(pubIndex);
    instance.budgets = buildBudgets(mean(instance.valuations), advertiserCapacities);
    
    % Draw an inventory process sample path
    instance.inventoryProcess = generateInventoryProcess(T, numPeriods, instance.invDim, inventoryMeans); 
end