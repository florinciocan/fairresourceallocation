function [ inventoryProcess ] = generateInventoryProcess(T, numPeriods, invDim, means)
% Generates a time series of inventory arrivals using AdX data
%
% INPUTS:
%   T:double             = length of time horizon
%   numPeriods:int       = discretization parameter. i.e. number of times 
%                          inventory OU process updates
%   invDim:int           = number of impression types
%   means:double[invDim] = array arrivals from Balseiro et al dataset 
%
% OUTPUTS: 
%   inventory:double[invDim, numSamples] = array with time series of the
%                                          impression inventory processes
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    lowerBound = 0.001; % Lower bound to ensure process rate does not reach 0
    inventoryProcess = zeros(invDim, numPeriods);
    % Draw sigma and theta conditionally on mu = mean based on AdX impression arrivals distribution
    sigmas = 10 * exp(-2.5952663 + 1.1811331 * log(means) + normrnd(0,0.6386464^2));
    thetas = 0.5154128  + 0.1042418 * log(means) + normrnd(0,1.1339179^2);
    % Generate OU time series using drawn OU process parameters
    unboundedOU = means;
    for t = 1:numPeriods
        unboundedOU = unboundedOU - T/numPeriods * (thetas .* (unboundedOU - means)) + sqrt(T/numPeriods) * (sigmas .* normrnd(0, 1, 1, invDim));
        inventoryProcess(:, t) = lowerBound * ones(invDim, 1) + abs(unboundedOU');
    end
end
        