clear close all

numSamples = 25;
T = 1;
numPeriods = 100;
numReopts = 10;
budgetScalingFactors = [1];
No_budgetScalingFactors  = length(budgetScalingFactors);

% run bc
FAIR_bc = zeros(No_budgetScalingFactors, 6);
EFFICIENCY_bc = zeros(No_budgetScalingFactors, 6);

for n = 1:No_budgetScalingFactors
     [FAIR_bc(n,1), EFFICIENCY_bc(n,1)] = runOnePublisherSimulation('bc', 1, 'pub1-ads.txt', 'pub1-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
     [FAIR_bc(n,2), EFFICIENCY_bc(n,2)] = runOnePublisherSimulation('bc', 2, 'pub2-ads.txt', 'pub2-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
     [FAIR_bc(n,3), EFFICIENCY_bc(n,3)] = runOnePublisherSimulation('bc', 3, 'pub3-ads.txt', 'pub3-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
     [FAIR_bc(n,4), EFFICIENCY_bc(n,4)] = runOnePublisherSimulation('bc', 4, 'pub4-ads.txt', 'pub4-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
     [FAIR_bc(n,5), EFFICIENCY_bc(n,5)] = runOnePublisherSimulation('bc', 5, 'pub5-ads.txt', 'pub5-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
     [FAIR_bc(n,6), EFFICIENCY_bc(n,6)] = runOnePublisherSimulation('bc', 6, 'pub6-ads.txt', 'pub6-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
end

% % run pf
FAIR_pf = zeros(No_budgetScalingFactors, 6);

for n = 1:No_budgetScalingFactors
%    FAIR_pf(n,1) = ru7 nOnePublisherSimulation('pf', 1, 'pub1-ads.txt', 'pub1-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
%    FAIR_pf(n,2) = runOnePublisherSimulation('pf', 2, 'pub2-ads.txt', 'pub2-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
%    FAIR_pf(n,3) = runOnePublisherSimulation('pf', 3, 'pub3-ads.txt', 'pub3-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
%    FAIR_pf(n,4) = runOnePublisherSimulation('pf', 4, 'pub4-ads.txt', 'pub4-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
%    FAIR_pf(n,5) = runOnePublisherSimulation('pf', 5, 'pub5-ads.txt', 'pub5-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
%    FAIR_pf(n,6) = runOnePublisherSimulation('pf', 6, 'pub6-ads.txt', 'pub6-types.txt', numSamples, T, numPeriods, numReopts, budgetScalingFactors(n))
end