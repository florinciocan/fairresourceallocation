function [clairvoyantOneShotUtility] = propFairClairvoyant(invDim, demandDim, T, valuations, inventory, x, numPeriods)
% Runs proportional fairness offline scheme for one sample path of the inventory processes.
%
% INPUTS:
%   T:double       = length of time horizon
%   numPeriods:int = discretization parameter. i.e. number of times inventory OU
%                    process updates
%   numReopts:int  = discretization paramter of re-optimization, i.e. number of
%                    times scheme re-optimizes
%   invDim:int     = number of impression types
%   demandDim:int  = number of advertisers
%   valuations:
%       double[invDim, demandDim]  = array of valuations
%   inventory:
%       double[invDim, numPeriods] = array containing timeseries of
%                                    impression arrival processes
%   budget:double[demandDim]       = array of advertiser budgets
%
% OUTPUTS: 
%   clairvoyantOneShotUtility:
%        double[demandDim] = array with advertiser utilities garnered by offline scheme
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    deltaT = T / numPeriods;
    buyerUtilities = zeros(demandDim, 1);
    currAllocation = zeros(invDim, demandDim);
    currPrices = zeros(invDim, 1);
    currUtilities = zeros(demandDim, 1);
    [currAllocation, currPrices, currUtilities] = solveEGProgram(x, inventory * deltaT, valuations, invDim, demandDim);
    buyerUtilities = (currAllocation .* valuations)' * inventory * deltaT;
    clairvoyantOneShotUtility = buyerUtilities;
end