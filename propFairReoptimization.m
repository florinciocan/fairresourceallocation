function [algOneShotUtility] = propFairReoptimization(T, numPeriods, numReopts, invDim, demandDim, valuations, inventory, budget)
% Runs proportional fairness re-optimization scheme for one sample path of the inventory processes.
%
% INPUTS:
%   T:double       = length of time horizon
%   numPeriods:int = discretization parameter. i.e. number of times inventory OU
%                    process updates
%   numReopts:int  = discretization paramter of re-optimization, i.e. number of
%                    times scheme re-optimizes
%   invDim:int     = number of impression types
%   demandDim:int  = number of advertisers
%   valuations:
%       double[invDim, demandDim]  = array of valuations
%   inventory:
%       double[invDim, numPeriods] = array containing timeseries of
%                                    impression arrival processes
%   budget:double[demandDim]       = array of advertiser budgets
%
% OUTPUTS: 
%   algOneShotUtilitity:
%       double[demandDim] = array with advertiser utilities garnered by scheme
% 
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    buyerUtilities = zeros(demandDim, 1);
    deltaT = T / numPeriods;
    timeDiff = numPeriods / numReopts;
    currAllocation = zeros(invDim, demandDim);
    currPrices = zeros(invDim, 1);
    currUtilities = zeros(demandDim, 1);
    remaining_budget = budget;
    
    for i = 0 : (numReopts - 1)
        sprintf('Running ** %d th** reoptimization', i)
        currT = i * timeDiff * deltaT;       
        [currAllocation, currPrices, currUtilities] = solveEGProgram(remaining_budget, inventory(:, i * timeDiff + 1) * (T - currT), valuations, invDim, demandDim);
        buyerUtilities = buyerUtilities + (currAllocation .* valuations)' * sum(inventory(:, i  * timeDiff + 1:(i + 1)  * timeDiff)')' * deltaT;
        remaining_budget = remaining_budget - remaining_budget / numReopts;
    end
    algOneShotUtility = buyerUtilities;
end