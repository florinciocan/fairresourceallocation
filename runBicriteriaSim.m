function [ algFairness, algEfficiency ] = runBicriteriaSim(numSamples, T, numPeriods, numReopts, invDim, demandDim, valuations, inventoryMeans, budgets)
% Function runs the simulation for the bi-criteria scheme over
% multiple sample paths of the impression inventory processes.
%
% INPUTS
% numSamples:int = number of sample paths of the inventory processes that we
%                  simulate
% T:double       = length of time horizon
% numPeriods:int = discretization parameter of OU inventory process, i.e.
%                  number of times process updates
% numReopts:int  = discretization paramter of re-optimization, i.e. number of
%                  times scheme re-optimizes
% invDim:int     = number of impression types
% demandDim:int  = number of advertisers
% valuations:
%     double[invDim, demandDim] = array of valuations
% inventoryMeans:int[invDim]    = array arrivals from Balseiro et al dataset
% budgets:int[demandDim]        = array of advertiser budgets
%
% OUTPUTS
% algFairness:double   = proportional fairness ratio of the re-optimization 
%                        scheme averaged out over sample paths
% algEfficiency:double = efficiency ratio of the re-optimization scheme
%                        averaged out over sample paths
%
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    sumFairness = 0.0;
    sumEfficiency = 0.0;
    for n = 1:numSamples
        sprintf('Running ** %d th** sample path', n)
        [ inventoryProcess ] = generateInventoryProcess(T, numPeriods, invDim, inventoryMeans);
        reoptUtil = bicriteriaReoptimization(numPeriods, numReopts, invDim, demandDim, T, valuations, inventoryProcess, budgets);
        [clairvoyantPropFairUtil, clairvoyantEffUtil] = bicriteriaClairvoyant(numPeriods, invDim, demandDim, T, valuations, sum(inventoryProcess')', budgets);       
        propFairRatio = 1;
        for i = 1:demandDim
            propFairRatio = propFairRatio * (reoptUtil(i)/clairvoyantPropFairUtil(i))^(budgets(i) / norm(budgets,1));
        end
        sumFairness = sumFairness + propFairRatio;
        effRatio = sum(reoptUtil) / sum(clairvoyantEffUtil);
        sumEfficiency = sumEfficiency + effRatio;
    end
    algFairness = sumFairness / numSamples;
    algEfficiency = sumEfficiency / numSamples;
    sprintf('FAIR = %f', algFairness)
    sprintf('EFFICIENCY = %f', algEfficiency)
end