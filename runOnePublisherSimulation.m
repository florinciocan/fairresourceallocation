function [fairnessRatio, efficiencyRatio] = runOnePublisherSimulation(mode, pubIndex, adfile, typefile, numSamples, T, numPeriods, numReopts, budgetScalingFactors)
% Function runs either proportional fairness or bi-criteria simulation for one publisher's dataset.
% 
% INPUTS:
%   mode:string     = 'pf' if running propportional fairness scheme, 'bc' if running
%                     bi-criteria scheme
%   pubIndex:int    = index of pubisher ranging from 1 to 6 from Balseiro et al
%                     dataset
%   adfile:string   = path to adfile containing advertiser information from Balseiro et al
%                     dataset
%   typefile:string = path to typefile containing impression type information from Balseiro et al
%                     dataset
%   numSamples:int  = number of sample paths of the inventory processes that we
%                     simulate
%   numPeriods:int  = discretization parameter of OU inventory process, i.e.
%                     number of times process updates
%   numReopts:int   = discretization paramter of re-optimization, i.e. number of
%                     times scheme re-optimizes
%
% OUTPUTS: 
%   fairnessRatio:double   = ratio of scheme's proportional fairness vs offline optimal
%   efficiencyRatio:double = efficiency of scheme vs offline optimal
%
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    rng(1); % Set RNG seed
    addpath 'adx-alloc-data-2014'
    [ demandDim, invDim, rho, arrival_prob, adjacencyMat, quality ] = LoadSynthFile( adfile, typefile );
    [ valuations ] = List2Matrix(adjacencyMat, quality);
    publisherN = [1500000, 2100000, 320000, 930000, 1800000, 6700000, 7000000];
    advertiserCapacities = rho * publisherN(pubIndex);
    inventoryMeans = arrival_prob * publisherN(pubIndex);
    fairnessRatio = 0 * budgetScalingFactors;
    efficiencyRatio = 0 * budgetScalingFactors;    
    for l = 1:size(budgetScalingFactors, 2)
        [ budgets ] = buildBudgets(mean(valuations), advertiserCapacities);   
        if strcmp(mode, 'pf') == 1
            fairnessRatio(l) = runPropFairSim(numSamples, T, numPeriods, numReopts, invDim, demandDim, valuations, inventoryMeans, budgets); 
        end
        if strcmp(mode, 'bc') == 1
            [fairnessRatio(l), efficiencyRatio(l)] = runBicriteriaSim(numSamples, T, numPeriods, numReopts, invDim, demandDim, valuations, inventoryMeans, budgets); 
        end
    end
end