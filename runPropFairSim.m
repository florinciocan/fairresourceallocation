function [ algFairness ] = runPropFairSim(numSamples, T, numPeriods, numReopts, invDim, demandDim, valuations, inventoryMeans, budgets)
% Function runs the simulation for the proportional fairness scheme over
% multiple sample paths of the impression inventory processes.
%
% INPUTS
% numSamples:int = number of sample paths of the inventory processes that we
%                  simulate
% T:double       = length of time horizon
% numPeriods:int = discretization parameter of OU inventory process, i.e.
%                  number of times process updates
% numReopts:int  = discretization paramter of re-optimization, i.e. number of
%                  times scheme re-optimizes
% invDim:int     = number of impression types
% demandDim:int  = number of advertisers
% valuations:
%     double[invDim, demandDim] = array of valuations
% inventoryMeans:int[invDim]    = array arrivals from Balseiro et al dataset
% budgets:int[demandDim]        = array of advertiser budgets
%
% OUTPUTS
% algFairness:double = proportional fairness ratio of the re-optimization 
%                      scheme averaged out over sample paths
%
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    sumFairness = 0.0;
    for n = 1 : numSamples
        sprintf('Running ** %d th** sample path', n)
        [ inventoryProcess ] = generateInventoryProcess(T, numPeriods, invDim, inventoryMeans);
        reoptUtil = propFairReoptimization(T, numPeriods, numReopts, invDim, demandDim, valuations, inventoryProcess, budgets);
        clairvoyantUtil = propFairClairvoyant(invDim, demandDim, T, valuations, sum(inventoryProcess')', budgets, numPeriods);
        %algOneShotFairness = exp(budgets' * log(reoptOneShotUtility)) / exp(budgets' * log(clairvoyantOneShotUtility));
        ratio = 1.0;
        for i = 1:demandDim
            ratio = ratio * (reoptUtil(i)/clairvoyantUtil(i))^(budgets(i) / norm(budgets, 1));
        end
        sumFairness = sumFairness + ratio;
    end
    algFairness = sumFairness / numSamples;
    sprintf('FAIR = %f', algFairness)
end