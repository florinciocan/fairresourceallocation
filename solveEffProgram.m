function [currAllocation, currPrices, currUtilities] = solveEffProgram(inventory, valuations, invDim, demandDim)
% Solves an instance of an efficiency optimization program
%
% INPUTS:
%   budget:
%       double[demandDim]          = array of advertiser budgets
%   inventory:
%       double[invDim, numPeriods] = array with time series of the
%                                    impression inventory processes
%   valuations:
%       double[invDim, demandDim]  = array with valuation of advertiser j 
%                                    for impression type i
%   invDim:int                     = number of impression types
%   demandDim:int                  = number of advertisers
%
% OUTPUTS: 
%   currAllocation:
%       double[invDim, demandDim] = array giving allocation of impression 
%                                   type i to advertiser j
%   currPrices:
%       double[invDim]            = array giving prices produced by EG program
%   currUtilities:
%       double[demandDim]         = array giving aggregate utility garnered by
%                                   advertiser
%
% Authors: MH Bateni, Y Chen, DF Ciocan and V Mirrokni

    if sum(inventory) > 0
        cvx_begin
            cvx_precision high;
            cvx_quiet true;

            variable z(invDim, demandDim);
            variable u(demandDim);
            dual variable p;
            
            tMat = repmat( inventory, 1, demandDim );
            prodMat = valuations .* tMat;

            maximize (sum(sum(prodMat .* z)))
            subject  to 
                p: sum(z')' <= ones(invDim, 1); 
                z  >= zeros(invDim, demandDim);
        cvx_end

        currUtilities = u;
        currAllocation = z;
        currPrices = p;
    else
        currUtilities = zeros(demandDim, 1);
        currAllocation = zeros(invDim, demandDim);
        currPrices = zeros(invDim, 1);
    end
end